package lab9;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class X_O extends JFrame{
	
	static JTextArea board[][] = new JTextArea[3][3];
	static char k = 0; //will remember who moves (0 -> X  and  1 -> O)
	JButton reset = new JButton();
	
	X_O(){
		setTitle("X & O game");
		setSize(309, 388);

		init();
		
		setLocationRelativeTo(null);  //il pune pe mijloc
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void init(){
		this.setLayout(null);
		int width=95; int height = 95;
		Border border = BorderFactory.createLineBorder(Color.BLUE, 5);
		
		board[0][0] = new JTextArea();
		board[0][0].setBounds(0, 0, width, height);
		board[0][0].setBorder(border);
		board[0][0].setFont(board[0][0].getFont().deriveFont(65f));  // make the text bigger
		board[0][0].addMouseListener(new MouseClickEvent(0, 0));
		
		board[0][1] = new JTextArea();
		board[0][1].setBounds(100, 0, width, height);
		board[0][1].setBorder(border);
		board[0][1].addMouseListener(new MouseClickEvent(0, 1));
		board[0][1].setFont(board[0][1].getFont().deriveFont(65f));  
		
		board[0][2] = new JTextArea();
		board[0][2].setBounds(200, 0, width, height);
		board[0][2].setBorder(border);
		board[0][2].addMouseListener(new MouseClickEvent(0, 2));
		board[0][2].setFont(board[0][2].getFont().deriveFont(65f));  
		
		board[1][0] = new JTextArea();
		board[1][0].setBounds(0, 100, width, height);
		board[1][0].setBorder(border);
		board[1][0].addMouseListener(new MouseClickEvent(1, 0));
		board[1][0].setFont(board[1][0].getFont().deriveFont(65f)); 
		
		board[1][1] = new JTextArea();
		board[1][1].setBounds(100, 100, width, height);
		board[1][1].setBorder(border);
		board[1][1].addMouseListener(new MouseClickEvent(1, 1));
		board[1][1].setFont(board[1][1].getFont().deriveFont(65f)); 
		
		board[1][2] = new JTextArea();
		board[1][2].setBounds(200, 100, width, height);
		board[1][2].setBorder(border);
		board[1][2].addMouseListener(new MouseClickEvent(1, 2));
		board[1][2].setFont(board[1][2].getFont().deriveFont(65f));  
		
		board[2][0] = new JTextArea();
		board[2][0].setBounds(0, 200, width, height);
		board[2][0].setBorder(border);
		board[2][0].addMouseListener(new MouseClickEvent(2, 0));
		board[2][0].setFont(board[2][0].getFont().deriveFont(65f)); 
		
		board[2][1] = new JTextArea();
		board[2][1].setBounds(100, 200, width, height);
		board[2][1].setBorder(border);
		board[2][1].addMouseListener(new MouseClickEvent(2, 1));
		board[2][1].setFont(board[2][1].getFont().deriveFont(65f)); 
		
		board[2][2] = new JTextArea();
		board[2][2].setBounds(200, 200, width, height);
		board[2][2].setBorder(border);
		board[2][2].addMouseListener(new MouseClickEvent(2, 2));
		board[2][2].setFont(board[2][2].getFont().deriveFont(65f)); 
		
		reset = new JButton("RESET");
		reset.setBounds(5, 300, width*3, height/2);
		reset.setBackground(Color.ORANGE);
		reset.addActionListener(new ResetButton());
		
		add(board[0][0]); add(board[0][1]); add(board[0][2]);
		add(board[1][0]); add(board[1][1]); add(board[1][2]);
		add(board[2][0]); add(board[2][1]); add(board[2][2]);
		add(reset);
	}
	
	public static void main(String[] args) {
		new X_O();
	}
	
	public class ResetButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			for(int i = 0; i < 3; i++)
				for(int j = 0; j < 3; j++)
					board[i][j].setText(null);  // empty all text areas
			k = 0;		// first move will be X as always
		}
	}
	
	public class MouseClickEvent implements MouseListener {
		
		int i; int j;  //will remember which square was pressed
		
		//Special constructor for my X&O
		MouseClickEvent(int i, int j){
			this.i = i; this.j = j;
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			if(k == 0) {
				board[i][j].setText(" X");
				k++; // so that next move will be O
			}
			else {
				board[i][j].setText(" O");
				k--; // next move will be X
			}
		}
		
		//This part of the code literally does nothing its just for the MouseListener interface to work
		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}		
		
	}
}
