package lab9;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;

public class ShowFile extends JFrame{
	
	JLabel instructions;
	JTextField fileNameField;
	JTextArea printArea;
	JButton bGo;
	
	ShowFile(){
		setTitle("File Reader");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    init();
	    setSize(400,400);
	    setVisible(true);
	}
	
	public void init() {
		this.setLayout(null);
		
		instructions = new JLabel("Enter the name of a file: ");
		instructions.setBounds(20, 20, 200, 20);
		
		fileNameField = new JTextField();
		fileNameField.setBounds(160, 20, 150, 20);
		
		bGo = new JButton("GO");
		bGo.setBounds(310, 20, 55, 20);
		bGo.addActionListener(new PressGoButton());
		bGo.setBackground(Color.GREEN);
		
		printArea = new JTextArea();
		printArea.setBounds(10, 50, 365, 305);
		printArea.setBackground(Color.getHSBColor(25, 35, 50));
		
		add(instructions); add(fileNameField); add(bGo); add(printArea);
	}
	
	public static void main(String[] args) {
		new ShowFile();
	}
	
	class PressGoButton implements ActionListener {
		
			@Override
			public void actionPerformed(ActionEvent e){
				
				//get the file name from the JTextField and assign a reader
				String fileName = ShowFile.this.fileNameField.getText();
				String s1, s2 = new String();
				try {
					//read the content of the file into s2
					BufferedReader in = new BufferedReader( new FileReader(fileName));
					while((s1 = in.readLine())!=null) {
						s2 = s2 + s1 + "\n";
					}
					in.close();
				} catch (FileNotFoundException e1) {
					ShowFile.this.printArea.setText("Error: The file was not found! Enter a valid file name...");
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally{
				// now print s2 in the JTextArea
				ShowFile.this.printArea.append(s2);
				}
			}
	}
}


