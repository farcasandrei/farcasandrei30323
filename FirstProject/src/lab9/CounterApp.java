package lab9;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class CounterApp extends JFrame{
	
	JLabel instructions;
    JTextArea number;
    JButton bCount;
	static Integer count;
	static String printedText;
	
	CounterApp(){
		setTitle("Professional Counter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,400);
        setVisible(true);
	}
	
	public void init(){
		this.setLayout(null);
        int width=85; int height = 85;
        
        //just some text
        instructions = new JLabel("Press the button to count:");
        instructions.setBounds(10, 20, 200, height);
        
        //current number
        count = 0;
        printedText = count.toString();   
        number = new JTextArea(printedText);
        number.setBounds(75, 175, width, height);
        number.setText(" "); // just puts the text in a more central position cuz why not
        number.setFont(number.getFont().deriveFont(50f));  // make the text bigger
        
        //button
        bCount = new JButton("+1");
        bCount.setBounds(200, 175, width, height);
        bCount.addActionListener(new PressCountButton());   
        
        add(instructions); add(number); add(bCount);
	}
	
	public static void main(String[] args) {
		new CounterApp();
	}
	
	// increment when button is pressed
	class PressCountButton implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			count++;
			printedText = count.toString();
			number.setText(" ");  // delete the previous counter
			CounterApp.this.number.append(printedText);  // print the new counter
		}
	}
}
