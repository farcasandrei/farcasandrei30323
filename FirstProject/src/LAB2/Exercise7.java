package LAB2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
		public static void main(String[] args) {
			Random r = new Random();
			int n = r.nextInt(10);
			System.out.println("Try to guess the number. You have 3 attempts :))");
			Scanner in = new Scanner(System.in);
			boolean loose = true;
			for(int i = 0; i < 3; i++) {
				int a = in.nextInt();
				if(a < n) 
					System.out.println("Wrong!! Your number is too small");
				else
					if(a > n)
						System.out.println("Wrong!! Your number is too big");
					else
					{
						System.out.println("Correct!! You won!");
						loose = false;
					}
			}
		if(loose)
			System.out.println("You ran out of attempts. The number was: " + n);
		}
}