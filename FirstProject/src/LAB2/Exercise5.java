package LAB2;

import  java.util.* ;

public class Exercise5 {
	public static void main(String[] args){
        Random r = new Random();
        int[] a = new int[10];
        for (int i = 0; i < a.length; i++){
            a[i] = r.nextInt(100);
        }
        sortareBule(a);
        for(int i = 0; i < a.length; i++) {
        	System.out.println(a[i] + "; ");
        }
	}
	
	public static void sortareBule(int[] a) {
		boolean flag = false;
		for(int i = 0; i < a.length-1; i++) {
			for(int j = 0; j < a.length-i-1; j++) {
				if(a[j] > a[j+1]) {
					int aux = a[j];
					a[j] = a[j+1];
					a[j+1] = aux;
					flag = true;
				}
			}
		if(!flag)
			break;
		}
	}
}
