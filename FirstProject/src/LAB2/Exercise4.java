package LAB2;

import java.util.Scanner;

public class Exercise4 {
	public static void main(String[] args) {
		System.out.println("Enter the number of elements in the vector:");
		Scanner in = new Scanner(System.in);
	    int n = in.nextInt();
	    int[] vector = new int[n];
	    System.out.println("Enter the elements in order: ");
	    vector[0] = in.nextInt();
	    int maximum = vector[0];
	    for(int i = 1; i < n; i++)
	    {
	    	vector[i] = in.nextInt();
	    	if(vector[i] > maximum)
	    		maximum = vector[i];
	    }
	    System.out.println("Valoarea maxima introdusa este: " + maximum);
	}
}
