package LAB2;

import java.util.Scanner;

public class Exercise6 {
	public static void main(String[] args) {
		int n = 0, factorial = 1;
		System.out.println("Let n = ");
		Scanner in = new Scanner(System.in);
		n = in.nextInt();
		for(int i=2; i<=n; i++)
		{
			factorial=factorial * i;
		}
		System.out.println("n! calculated non recursive is = " + factorial);
		System.out.println("n! calculated in a recursive way is = " + recursiv(n));
	}	
	public static int recursiv(int n) {
		if(n != 0)
		{
			return n * recursiv(n-1);
		}
		return 1;
	}
}
