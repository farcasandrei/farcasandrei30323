package LAB2;

import java.util.Scanner;

public class Exercise3 {
	public static void main(String[] args) {
		System.out.println("Please enter two numbers in ascending order: ");
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		int b = in.nextInt();
		System.out.println("The prime numbers in the interval [" + a + ", " + b + "] are:3 ");
		int k = 0;
		for(int i = a; i <= b; i++)
		{
			if(isPrime(i)) 
			{
				System.out.println(i + ";");
				k++;
			}
		}
		System.out.println("There are " + k + " prime numbers.");
	}
	public static boolean isPrime(int x) {
		for(int i = 2; i <  x; i++)
			if(x % i == 0) return false;
		return true;
	}
}
