package lab7;

import java.io.*;

public class Encripter {
	
	public static void main(String[] args) throws IOException {
		Encripter e = new Encripter();
		switch(args[0]) {
			case "Encript":
			{
				String fileContent = e.readFile(args[1]);  // reads all info from the text file from args[1]
				args[1] = args[1].substring(0, args[1].length()-4);  // cuts the .txt from the path of the text file
				e.Encript(fileContent, args[1]);	// creates a .enc file with the same name as the original .txt
				break;
			}
			case "Decript":
			{
				String fileContent = e.readFile(args[1]);
				args[1] = args[1].substring(0, args[1].length()-4);
				e.Decript(fileContent, args[1]);	// creates a .enc file with the same name as the original .txt
				break;
			}
			default:
				System.out.println("The first argument should be Encript/Decript, followed a text file");
		}
	}
	
	private String readFile(String fileName) throws IOException{
		BufferedReader in = new BufferedReader( new FileReader(fileName));
		String s, s2 = new String();
		while((s = in.readLine())!=null)
			s2 += s + "\n";
		in.close();
		return s2;
	}
	
	private void Encript(String s, String fileName) throws IOException {
		String sEncripted = new String();
		for(int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			c++;
			sEncripted = sEncripted + c;
		}
		try {
			BufferedReader in = new BufferedReader(new StringReader(sEncripted));
			File myFile = new File(fileName + ".enc");				// Create a new file of .enc type
			System.out.println("A new .enc file has been created at the same location as the original file.");
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(myFile)));   // PrintWriter will write each line in the above created file
			String s1 = new String();  // "Container" for each line in the encripted string sEncripted
			while((s1 = in.readLine())!=null) // Reads each line from the container
				out.println(s1);  // Prints each line in the file, using the PrintWriter out
			in.close(); 
			out.close();
		}
		catch(EOFException e) {
			System.err.println("End of stream");
		}
	}
	
	private void Decript(String s, String fileName) throws IOException{
		String sDecripted = new String();
		for(int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			c--;
			sDecripted = sDecripted + c;
		}
		try {
			BufferedReader in = new BufferedReader(new StringReader(sDecripted)); // reader for 
			File myFile = new File(fileName + ".dec");   // creates .dec file
			System.out.println("A new .dec file has been created at the same location as the original file.");
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(myFile)));
			String s1 = new String();  // "container"
			while((s1 = in.readLine())!=null)   
				out.println(s1);		// writes in the new file each line from s1 that is read from sDecripted 
			in.close(); 
			out.close();
		}
		catch(EOFException e) {
			System.err.println("End of stream");
		}
	}
	
}
