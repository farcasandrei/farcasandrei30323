package lab7;
import java.io.*;
import java.util.*;

public class Garage implements Serializable {
	private static final long serialVersionUID = 1L;  // added by default due to implementing Serializable
	private ArrayList<Car> parkingSpots;
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Garage garaj = new Garage();
		garaj.menu();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int option = Integer.parseInt(in.readLine());
		while(option != 0) {
			switch(option) {
			case 1:
				//create a new car and add to array
				System.out.println("Please give the model name: ");
				String model = in.readLine();
				System.out.println("Please give further details: ");
				String details = in.readLine();
				System.out.println("Please give the price: ");
				int price = Integer.parseInt(in.readLine());
				garaj.parkingSpots.add(new Car(model, details, price));
				break;
			case 2:
				garaj.showAllCars();
				break;
			case 3:
				System.out.println("Enter the index of the car: ");
				garaj.showCar(Integer.parseInt(in.readLine()));
				break;
			case 4:
				garaj.saveGarage("data.txt");
				break;
			case 5:
				garaj = garaj.loadGarage("data.txt");
				break;
			default: 
				System.out.println("\n!! Invalid option please try again !!");
			}
		garaj.menu();
		option = Integer.parseInt(in.readLine());
		}
		in.close();
	}
	//constructor
	Garage(){
		this.parkingSpots = new ArrayList<Car>();
	}
	
	void parkCar(Car car) {
		this.parkingSpots.add(car);
	}
	
	void showAllCars() {
		System.out.println("The cars in your garage are: ");
		for(Car i : this.parkingSpots) {
			System.out.println(" Car number " + this.parkingSpots.indexOf(i) + ": " + i);
		}
	}
	
	void showCar(int index) {  //probably nicer with a map but its too late now
		Car car = this.parkingSpots.get(index);
		System.out.println("Details for car: " + car.getModel() + " "
				+ car.getDetails());
	}
	
	//serialization save the garage
	void saveGarage(String adress) throws IOException {
		ObjectOutputStream o = new ObjectOutputStream(
				new FileOutputStream(adress));
		o.writeObject(this);
		System.out.println("Garage saved sucessfully.");
		o.close();
	}
	
	//serialization load the garage
	Garage loadGarage(String adress) throws IOException, ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(adress));
		Garage g = (Garage)in.readObject();
		System.out.println("Garage loaded. You can now do operations on it.\n");
		in.close();
		return g;
	}
	
	public ArrayList<Car> getParkingSpots() {
		return parkingSpots;
	}
	
	public void addCarToGarage(Car car) {
		this.parkingSpots.add(car);
	}
	
	private void menu() {
		System.out.print("\n\tWELCOME TO YOUR VIRTUAL GARAGE\n"
				+ "  1) Add a car to your garage\n"
				+ "  2) View all cars from the garage\n"
				+ "  3) View one specific car with details\n"
				+ "  4) Save modifications\n"
				+ "  5) Load a previously saved garage.\n"
				+ "  0) Exit the app\n"
				+ "Type in your option: ");
	}
}
