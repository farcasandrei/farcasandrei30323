package lab7;
import java.io.*;

public class Car implements Serializable{
	private String model, details;
	private int price;
	
	public Car(String model, String details, int price) {
		this.model = model;
		this.details = details;
		this.price = price;
	}
	
	public String getModel() {
		return model;
	}
	public String getDetails() {
		return details;
	}
	
	@Override
	public String toString() {
		return this.model; 
	}
}
