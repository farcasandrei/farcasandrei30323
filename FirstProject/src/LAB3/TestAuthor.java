package LAB3;

public class TestAuthor {
	public static void main(String[] args) {
		Author autor = new Author("Eminescu", "eminescu_mihai@yahoo.com", 'm');
		
		System.out.println("The author's name is: " + autor.getName());
		System.out.println("The author's gender is: " + autor.getGender());
		System.out.println("The author's old email is: " + autor.getEmail());
		autor.setEmail("eminescu2022@yahoo.com");
		System.out.println(autor.toString());
	}
}
