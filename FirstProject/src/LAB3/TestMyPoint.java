package LAB3;

public class TestMyPoint {
	public static void main (String[] args) {

		MyPoint p1 = new MyPoint();
		MyPoint p2 = new MyPoint(3, 3);
	
		System.out.println("Distance between (0, 0) and (2, 4) is: " + p1.distance(2, 4));
		System.out.println("Distance between (0, 0) and (3,3) is: " + p1.distance(p2));
	}	
}
