package LAB3;

public class MyPoint {
	private int x, y;
	
	MyPoint(){
		this.x = 0;
		this.y = 0;
	}
	
	MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}
	
	public double distance(int x, int y) {
		return Math.sqrt((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y));
	}
	
	public double distance(MyPoint another) {
		return Math.sqrt((this.x-another.x)*(this.x-another.x) + (this.y-another.y)*(this.y-another.y));
	}
}
