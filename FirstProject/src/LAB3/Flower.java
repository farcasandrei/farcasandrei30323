package LAB3;

public class Flower{
    private int petal;
    private static int counter = 0;
    Flower(int p){       
      petal=p;
      System.out.println("New flower has been created!");
      counter++;
    }
    
    public static int getCounter() {
    	return counter;
    }
    public static void main(String[] args) {
    	Flower f1 = new Flower(4);
    	Flower f2 = new Flower(6);
    	System.out.println(Flower.getCounter() + " flowers have been created");
    	Flower f3 = new Flower(7);
    	System.out.println(Flower.getCounter() + " flowers have been created");
}
}
