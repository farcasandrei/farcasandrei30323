package LAB3;

public class TestCircle {
	public static void main(String[] args) {
		Circle c1 = new Circle();
		Circle c2 = new Circle(2.0, "blue");
		
		System.out.println("The radius of the " + c1.getColor() + " circle is: " + c1.getRadius());
		System.out.println("The radius of the " + c2.getColor() + " circle is: " + c2.getRadius());
		System.out.println("The area of the " + c2.getColor() + " circle is: " + c2.getArea());
	}
}
