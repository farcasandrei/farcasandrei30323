package LAB4.Shape;

public class TestShape {
	public static void main (String[] args) {
		Shape forma = new Shape("dark white", false);
		Circle cerc = new Circle(3.5, "orange", true);
		Shape dreptunghi = new Rectangle(2.0, 3.0, "blue", true);
		Rectangle patrat = new Square(2.0, "green", false);
		
		System.out.println(forma);
		System.out.println(cerc);
		System.out.println(dreptunghi);
		System.out.println(patrat);
		
		System.out.println("\n Area of circle is: " + cerc.getArea());
		System.out.println("\n Perimeter of square is: " + patrat.getPerimeter());
	}
}
