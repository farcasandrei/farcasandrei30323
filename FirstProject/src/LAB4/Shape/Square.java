package LAB4.Shape;

public class Square extends Rectangle {
	Square(){
		super();
	}
	
	Square(double side){
		super(side, side);
	}
	
	Square(double side, String color, boolean filled){
		super(side, side, color, filled);
	}
	
	public double getSide() {
		return this.getWidth();
	}
	
	public void setSide(double side) {
		this.setLength(side);
		this.setWidth(side);
	}
	
	@Override
	public void setWidth(double width) {
		this.setSide(width);
	}

	@Override
	public void setLength(double length) {
		this.setSide(length);
	}
	
	@Override
	public String toString() {
		return "A Square with side = " + this.getSide() + ", which is a subclass of " + super.toString();
	}
}
