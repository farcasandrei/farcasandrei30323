package LAB4;
import LAB3.Circle;

public class TestCylinder {
	public static void main(String[] args) {
		Circle c1 = new Cylinder(2.25, 4.50);
		System.out.println("Volume of c1 = " + ((Cylinder)c1).getVolume() + " m3"); // downcasting
		Cylinder c2 = new Cylinder(1.5, 1.5);
		System.out.println("Volume of c2 = " + c2.getVolume() + " m3");
		
		// Area testing 
		System.out.println("Area of c2 = " + c2.getArea() + " m2");
	}
}