package LAB4;
import LAB3.Author;
public class Book {
	private String name;
	private Author autor;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author author, double price) {
		this.name = name;
		this.autor = author;
		this.price = price;
	}
	
	public Book(String name, Author author, double price, int qtyInStock) {
		this.name = name;
		this.autor = author;
		this.price = price;
		this.qtyInStock = qtyInStock;
	}

	public String getName() {
		return name;
	}

	public Author getAutor() {
		return autor;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQtyInStock() {
		return qtyInStock;
	}

	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public String toString() {
		return "'" + this.getName() + "' by " + this.autor.toString();
	}
	
	// mine
	public void updateAuthorEmail(String email) {
		this.autor.setEmail(email);
	}
}
