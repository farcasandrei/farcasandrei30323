package LAB4;
import LAB3.Author;

public class TestBooks {
	public static void main (String[] args) {
		Author calinescu = new Author("Calinescu", "georgeC@yahoo.com", 'm');
		Book carte1 = new Book("Enigma Otiliei",calinescu, 35.00);
		Book carte2 = new Book("Trei nuvele", calinescu, 12.00);
		System.out.println(carte1);
		System.out.println(carte2);
		
		// personal experiment
		carte2.updateAuthorEmail("newCalinescu@gmail.com");
		System.out.println(carte2);
	}
}
