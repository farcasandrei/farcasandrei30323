package lab6;

import java.util.ArrayList;

public class BankAccount {
	private String owner;
	private double balance;
	
	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public String getOwner() {
		return owner;
	}

	public void withdraw(double amount) {
		this.balance -= amount;
	}
	
	public void deposit(double amount) {
		this.balance += amount;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount) {
			BankAccount cont = (BankAccount)obj;
			return (this.balance == cont.balance) && (this.owner.equals(cont.owner));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.balance + this.owner.hashCode();
	}

	@Override
	public String toString() {
		return "Owner: " + this.owner + " has a balance of: " + this.balance;
	}
	
	public static void sort(ArrayList<BankAccount> accounts, Object object) {
		// TODO Auto-generated method stub
	}
	
	/*//Test for equals method
	public static void main(String[] args) {
		BankAccount cont1 = new BankAccount("John", 2000);
		BankAccount cont2 = new BankAccount("Andrei", 2500);
		BankAccount cont3 = new BankAccount("John", 2000);
		
		System.out.println("cont1 = cont2: " + cont1.equals(cont2));
		System.out.println("cont1 = cont3: " + cont1.equals(cont3));
	}
	*/
}
