package lab6;

public class Definition {
	private String description;

	public Definition(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return this.description + " ";
	}
}
