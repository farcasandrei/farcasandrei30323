package lab6;

import java.util.ArrayList;

public class BankAccountEx3 implements Comparable{
	private String owner;
	private double balance;
	
	public BankAccountEx3(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public String getOwner() {
		return owner;
	}

	public void withdraw(double amount) {
		this.balance -= amount;
	}
	
	public void deposit(double amount) {
		this.balance += amount;
	}
	
	@Override
	public int compareTo(Object o) {
		BankAccountEx3 c = (BankAccountEx3)o;
		if(this.balance == c.balance) return 0;
		if(this.balance > c.balance) return 1;
		return -1;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccountEx3) {
			BankAccountEx3 cont = (BankAccountEx3)obj;
			return (this.balance == cont.balance) && (this.owner.equals(cont.owner));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.balance + this.owner.hashCode();
	}

	@Override
	public String toString() {
		return "Owner: " + this.owner + " has a balance of: " + this.balance;
	}
	
	public static void sort(ArrayList<BankAccount> accounts, Object object) {
		// TODO Auto-generated method stub
	}
}