package lab6;
import java.util.Scanner;
import java.util.ArrayList;

public class ConsoleMenu {
	public static void main(String args[]) {
		Dictionary d = new Dictionary();
		menu();
		Scanner in = new Scanner(System.in);
		Scanner in2 = new Scanner(System.in);
	    int o = in.nextInt();
	    while(o != 0) {
	    	switch(o) {
	    		case 1:
	    		{
	    			System.out.println("Now enter the new word: ");
	    			Word w = new Word(in.next());
	    			System.out.println("Now enter the definition: ");
	    			Definition def = new Definition(in2.nextLine());
	    			d.addWord(w, def); break;
	    		}
	    		case 2:
	    		{
	    			System.out.println("Now enter the desired word: ");
	    			Word w = new Word(in.next());
	    			Definition def = d.getDefinition(w);
	    			System.out.println(def); break;
	    		}
	    		case 3:
	    		{
	    			ArrayList<Word> w = new ArrayList<Word>();
	    			w = d.getAllWords();
	    			for(Object i : w) {
	    				System.out.print(i + " ");
	    			}
	    			System.out.print("\n");
	    			break;
	    		}
	    		default: 
	    			System.out.println("Inexistent option. Please try again!");
	    	}
		    menu();
	    	o = in.nextInt();
	    }
	    in.close();
	    in2.close();
	}
	private static void menu(){
		System.out.println("\tMENU\n"
				+ "1. Add a word to the dictionary\n"
				+ "2. Get the definition of any word\n"
				+ "3. Print a list of all words\n"
				+ "0. Exit\n"
				+ "Type in your desired option: ");	
	}
	private static String getInput() {
	    Scanner scanner = new Scanner(System.in);
	    return scanner.nextLine();
	}
}
