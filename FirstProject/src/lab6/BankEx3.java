package lab6;

import java.util.TreeSet;

public class BankEx3 {
	private TreeSet<BankAccountEx3> accounts = new TreeSet<BankAccountEx3>();
	
	public void addAccount(String owner, double balance) {
		this.accounts.add(new BankAccountEx3(owner, balance));
	}
	
	public void printAccounts() {
		for(BankAccountEx3 i : this.accounts) {
			System.out.println(i);
		}
	}
	
	public void printAccounts(double minBalance, double maxBalance) {
		for(BankAccountEx3 i : this.accounts) {
			if(i.getBalance() > minBalance && i.getBalance() < maxBalance) {
				System.out.println(i);
			}
		}
	}
	
	public BankAccountEx3 getAccount(String owner) {
		for(BankAccountEx3 i : this.accounts) {
			if(owner.equals(i.getOwner())) {
				return i;
			}
		}
		System.out.println("No such object found. Creating one with 0 balance right now...");
		return new BankAccountEx3(owner, 0);
	}
	
	public TreeSet<BankAccountEx3> getAllAccounts() {
		return accounts;
	}
	
	public static void main(String[] args) {
		BankEx3 bt = new BankEx3();
		bt.addAccount("Andrei", 1000);
		bt.addAccount("George", 200);
		bt.addAccount("Iohannis", 20000);
		System.out.println("printAccounts();");
		bt.printAccounts();
		System.out.println("printAccounts(100, 1100)");
		bt.printAccounts(100, 1100);
		System.out.println("bt.getAccount(\"Andrei\");\n"+ bt.getAccount("Andrei"));
		
	}
}

