package lab6;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;

public class Dictionary {
	private HashMap<Word, Definition> dictionary;
	
	public Dictionary() {
		this.dictionary = new HashMap<Word, Definition>();
	}
	
	public void addWord(Word w, Definition d) {
		dictionary.put(w, d);
	}
	
	public Definition getDefinition(Word w) {
		return dictionary.get(w);
	}
	
	public ArrayList<Word> getAllWords() {
		ArrayList<Word> words = new ArrayList<Word>();
		Iterator<Word> iterator = dictionary.keySet().iterator();
		while(iterator.hasNext()) {
			words.add(iterator.next());
		}
		return words;
	}

	public ArrayList<Definition> getAllDefinitions() {
		ArrayList<Definition> definitions = new ArrayList<Definition>();
		Iterator<Word> iterator = dictionary.keySet().iterator();
		while(iterator.hasNext()) {
			definitions.add(getDefinition(iterator.next()));
		}
		return definitions;
	}
}
