package lab6;

import java.util.*;

public class Bank {
	private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();
	
	public void addAccount(String owner, double balance) {
		accounts.add(new BankAccount(owner, balance));
	}
	
	public void printAccounts() {
		Collections.sort(accounts, (first, second) -> Double.compare(first.getBalance(), second.getBalance()));
		for(int i = 0; i < accounts.size(); i++) {
			System.out.println(accounts.get(i));
		}
	}
	
	public void printAccounts(double minBalance, double maxBalance) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getBalance() > minBalance && accounts.get(i).getBalance() < maxBalance) {
				System.out.println(accounts.get(i));
			}
		}
	}
	
	public BankAccount getAccount(String owner) {
		for(int i = 0; i < accounts.size(); i++) {
			if(owner.equals(accounts.get(i).getOwner()))
				return (BankAccount)accounts.get(i);
		}
		System.out.println("No such object found. Creating one with 0 balance right now...");
		return new BankAccount(owner, 0);
	}
	
	public ArrayList getAllAccounts() {
		return accounts;
	}
	
	public static void main(String[] args) {
		Bank bt = new Bank();
		bt.addAccount("Andrei", 1000);
		bt.addAccount("George", 200);
		bt.addAccount("Iohannis", 20000);
		System.out.println("printAccounts();");
		bt.printAccounts();
		System.out.println("printAccounts(100, 1100)");
		bt.printAccounts(100, 1100);
		System.out.println("bt.getAccount(\"Andrei\");\n"+ bt.getAccount("Andrei"));
		
	}
}
