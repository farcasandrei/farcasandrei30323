package lab5.ex1;

abstract class Shape {
	private String color = "red";
	private boolean filled = true;
	
	protected Shape(){
		this.color = "green";
		this.filled = true;
	}
	
	protected Shape(String color, boolean filled){
		this.color =  color;
		this.filled = filled;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}
	
	public String toString() {
		if(this.filled)
			return "A shape of color " + this.getColor() + " and filled";
		else 
			return "A shape of color " + this.getColor() + " and not filled";
	}
	
	abstract double getArea();
	abstract double getPerimeter();
	
}

