package lab5.ex1;

public class TestShape {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[3];
		int i = 0;
		shapes[i++] = new Circle(2.5, "dark white", false);
		shapes[i++] = new Rectangle(1.3, 4.5);
		shapes[i++] = new Square(2);
		for(i = 0; i < 3; i++) {
			System.out.println(shapes[i] + ", has an area of: " + (double)Math.round(shapes[i].getArea()*100)/100 + " and a perimeter of: " + (double)Math.round(shapes[i].getPerimeter()*100)/100);
		}
	}
}
