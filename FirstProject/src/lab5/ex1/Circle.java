package lab5.ex1;

public class Circle extends Shape {
	protected double radius = 1.0;

	public Circle() {
		super();
		this.radius = 1.0;
	}
	
	public Circle(double radius) {
		this.radius = radius;
		this.setColor("red");
		this.setFilled(true);
	}
	
	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getPerimeter() {
		return 2 * this.radius * 3.14;
	}
	
	public double getArea() {
		return this.radius * this.radius * 3.15;
	}
	
	@Override
	public String toString() {
		return "A circle of radius = " + this.radius + ", which is a subclass of " + super.toString();
	}
}
