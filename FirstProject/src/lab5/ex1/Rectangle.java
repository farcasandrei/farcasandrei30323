package lab5.ex1;

public class Rectangle extends Shape {
	protected double width;
	protected double length;
	
	Rectangle(){
		super();
		this.length = 1.0;
		this.width =  1.0;
	}
	
	Rectangle(double width, double length){
		super();
		this.width = width;
		this.length = length;
	}
	
	Rectangle(double width, double length, String color, boolean filled){
		super(color, filled);
		this.width = width;
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public double getArea() {
		return this.getLength() * this.getWidth();
	}
	
	public double getPerimeter() {
		return 2 * this.getLength() + 2 * this.getWidth();
	}
	
	@Override
	public String toString() {
		return "A rectangle of width = " + this.getWidth() + " and length = " + this.getLength() + ", which is a subclass of " + super.toString();
	}
}

