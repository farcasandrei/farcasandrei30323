package lab5.ex3;
import java.util.Random;

public class TemperatureSensor extends Sensor{
	
	public int readValue() {
		Random r = new Random();
		int n = r.nextInt(100);
		return n;
	}
}
