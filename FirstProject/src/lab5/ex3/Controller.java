package lab5.ex3;

public class Controller {
	public TemperatureSensor tempSensor = new TemperatureSensor();
	public LightSensor lightSensor = new LightSensor();
	
	public void control() {
		for(int i = 0; i < 20; i++) {
			System.out.print("Second: " + (i+1) + "\t temperature: " + tempSensor.readValue());
			System.out.println(" \t light: " + lightSensor.readValue());
			wait(1000);
		}
		
	}
	
	public static void wait(int ms)
	{
	    try
	    {
	        Thread.sleep(ms);
	    }
	    catch(InterruptedException ex)
	    {
	        Thread.currentThread().interrupt();
	    }
	}
}
